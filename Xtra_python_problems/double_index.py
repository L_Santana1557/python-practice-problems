# Create a fxn named “double_index” with two parameters: a list and an index number.
# The fxn should double the value of the list element at the specified index and
# return the list with the doubled value.
# If the index is not a valid index, the fxn should return the original list.


def double_index(list, index):
    if index < len(list):
        list[index] *= 2
    return list


#                  <   list    >  <index>
print(double_index([3, 8, -10, 12], 2))  # Output: [3, 8, -20, 12]

# print(double_index([3, 8, -10, 12, 16, 19, 22], 6))  # Output: [3, 8, -10, 12]
