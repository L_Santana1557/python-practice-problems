# APPEND SIZE
# Create a fxn named “append_size” that has one parameter: a list.
# The fxn should add all of the numbers between 1 and the size of the list to
# the end of the list,
# and then return the list.

# Input: ([23,42,108]) => Output: [23,42,108,1,2,3]


def append_size(list):
    list_length = len(list)
    for numbers in range(1, list_length + 1):
        list.append(numbers)
    return list


print(append_size([23, 42, 108, 12, 12, 12]))
