# REVERSED LIST
# Create a fxn named “reversed_list” that has 2 parameters: 2 lists.
# If the first list is the same as the second list reversed, the fxn should return “True”.
# If not, it should return “False”.

# Input: ([1,2,3],[3,2,1]) => Output: True
# Input: ([1,5,3],[3,2,1]) => Output: False


def reversed_list(list_1, list_2):
    if list_1 == list(reversed(list_2)):
        return True
    else:
        return False


lst1 = [1, 2, 3]
lst2 = [3, 2, 1]

print(reversed_list(lst1, lst2))
