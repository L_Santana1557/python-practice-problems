# EVERY THREE NUMS
# Create a fxn named “every_three_nums” that has one parameter: a number.
# The fxn should return a list of every third number between the number
# passed in and 100 (inclusive). If the number passed in is greater then 100,
# it should return and empty list.

# Input: (91) => Output: [91, 94,97,100]
# Input: (106) => Output: []


def ever_three_num(a_number):
    empty_list = []
    if a_number > 100:
        return empty_list
    else:
        return list(range(a_number, 101, 3))


print(ever_three_num(91))
