# APPEND SUM
# Create a fxn named “append_sum” that has one parameter: a list.
# The fxn should add the last two elements of the list together and
# add the result to the end of the list. It should perform this process three
# times and then return the list.

# Input: ([1,1,2]) => Output: [1,1,2,3,5,8]


def append_sum(list):
    # add the last 2 things in the list
    # then append the result to that same list
    # perform that 3 times
    # and return the final list

    for item in range(3):
        result = list[-1] + list[-2]
        list.append(result)
    return list


print(append_sum([1, 1, 2]))  # Output: [1,1,2,3,5,8]
