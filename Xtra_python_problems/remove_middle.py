# Create a fxn named “remove_middle” which has three parameters: a list, a start number, an end number.
# The fxn should return a sub-list of the list containing all of the elements between the start and end indexes.

# Input: ([4,8,15,16,23,42], 1, 3]) => Output: [4,23,42]


# def remove_middle(list, start_num, end_num):
#     return list[start_num:end_num+1]


# print(remove_middle([4, 8, 15, 16, 23, 42], 1, 3))  # Output: [8, 15, 16]


def remove_middle(list, start_num, end_num):
    index = 0
    new_list = []
    for item in list:

        if index >= start_num and index <= end_num:
            new_list.append(item)

        index += 1

    return new_list


print(remove_middle([4, 8, 15, 16, 23, 42], 1, 3))
