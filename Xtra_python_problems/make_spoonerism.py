# MAKE SPOONERISM
# Create a fxn named “make_spoonerism” that has 2 parameters: 2 strings.
# The fxn should return the two words with the first letters switched.

# Input: (“Hello”, “John”) => Output: Jello Hohn


# def make_spoonerism(string1, string2):
#     string1 = string2[0] + string1[1:]
#     string2 = string1[0] + string2[1:]
#     return string1 + " " + string2


# print(make_spoonerism("Hello", "John"))

def make_spoonerism(string1, string2):
    index1 = string1[0]
    index2 = string2[0]

    var1 = string1.replace(index1, index2)
    var2 = string2.replace(index2, index1)
    return var1 + " " + var2


print(make_spoonerism("Hello", "John"))  # => Output: Jello Hohn
