# MAX NUM
# Create a fxn called “max_num” that has one parameter: a list of numbers.
# The fxn should return the largest number in the list

# Input: ([-10,0,20,50,75,15]) => Output: 75


def max_num(list1):
    return max(list1)


print(max_num([-10,0,20,50,75,15]))
