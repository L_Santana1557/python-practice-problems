# GREETINGS
# Create a fxn named “greetings” that has one parameter: a list of names.
# In the fxn add the string “Hello” in front of each name and return the list.

# Input: ([“Sam”, “Jim”, “Sophie”]) => Output: [“Hello, Sam”, “Hello, Jim”, “Hello, Sophie”]


def greetings(lst):
    return ["Hello, " + name for name in lst]


print(greetings(["Sam", "Jim", "Sophie"]))
