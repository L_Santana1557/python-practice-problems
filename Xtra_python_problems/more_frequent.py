# MORE FREQUENT ITEM
# Create a fxn named “most_frequent_item” that has three parameters: a list, a first item, a second item.
# Return either the first item or the second item depending on which occurs more often in the list.
# If the two items appear the same number of times, return the first item.

# Input: ([2,3,3,2,3,2,3,2,3], 2, 3) => Output: 3
# Input: ([2,3,3,2,3,2,3,2], 2, 3) => Output: 2


def most_frequent_item(list, first_item, second_item):
    for item in list:
        if list.count(first_item) > list.count(second_item):
            return first_item
        elif list.count(first_item) < list.count(second_item):
            return second_item
        else:
            return first_item


print(most_frequent_item([2, 3, 3, 2, 3, 2, 3, 2, 3], 2, 3))    # => Output: 3
