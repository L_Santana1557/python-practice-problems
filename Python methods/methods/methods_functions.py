# Use the append method to add "orange" to the fruits list.

fruits = ["apple", "banana", "cherry"]
fruits.append("orange")

# Use the insert method to add "lemon" as the second item in the fruits list.

fruits = ["apple", "banana", "cherry"]
fruits.insert(1,"lemon")

# Use the remove method to remove "banana" from the fruits list.

fruits = ["apple", "banana", "cherry"]
fruits.remove("banana")

# Use the correct syntax to print the number of items in the list.

fruits = ["apple", "banana", "cherry"]
print(len(fruits))

# Return the string without any whitespace at the beginning or the end.
# .strip removes the first and last char in original string

txt = " Hello World "
x = txt.strip()

# Use the add method to add "orange" to the fruits set.

fruits = {"apple", "banana", "cherry"}
fruits.add("orange")
