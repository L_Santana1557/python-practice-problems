# Make pairs
# Please complete the pair_up function below so that it takes a
# list of items and returns a list of pairs of consecutive items. Here's an example:

# input = [1,2,3,4,5,6,7]

# pair_up(input)  # Returns [[1,2], [3,4], [5,6]]
# Note: since there were an odd number of items,
# the 7 didn't have a partner to pair with, so it was excluded.

# Think through this problem. It may seem complex, but you can do this.

# What gets returned for an empty list?
# Can you do this with a single if statement?
# Can you do this with a for loop?
# Answer


def pair_up(items):
    # your code here
    pairs = []                                  #-hide
    pair0 = items[::2]                          #-hide
    pair1 = items[1::2]                         #-hide
    for idx in range(len(pair1)):               #-hide
        pairs.append([pair0[idx], pair1[idx]])  #-hide
    return pairs                                #-hide


def pair_up(items):
    res = []
    temp = []
    for item in items:
        temp.append(item)
        if len(temp) == 2:
            res.append(temp)
            temp = []
    return res
