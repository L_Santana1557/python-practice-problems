# A mean problem
# The following function is failing the unit tests with a divide by zero
# error when called like this:

# numbers = []
# mean(numbers) # --> ZeroDivisionError: division by zero
# If this situation is detected, It should return float('nan').

# Update the code below to handle this situation and pass the failing unit test.

Answer
def mean(numbers):
    if len(numbers) == 0:
        return float('nan')
    _sum = sum(numbers)
    return  _sum / len(numbers)
