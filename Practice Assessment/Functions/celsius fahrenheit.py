Celsius to fahrenheit
This function converts temperature measured
in degrees Celsius (°C) to degrees Fahrenheit (°F). Here are two examples:

°C	°F
0	32
100	212
The formula is:

°F = (°C * 9/5) + 32

Answer

def c_to_f(degrees_celsius):
    # your code here
    return degrees_celsius * 9 / 5 + 32 #-hide
