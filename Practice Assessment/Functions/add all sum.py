Implement the function add_all(), which returns the sum of all of its arguments.

Sample inputs/outputs:

*numbers	output

(none)	    0
3	        3
1, 2, 3	    6

Answer


def add_all(*numbers):
    # your code here
    sum = 0
    for number in numbers:
        sum += number
    return sum
