# Delimited files
# This function must take a string
# that's composed of values that are separated by some "delimiter" character,
# like a comma. It will return an list of the values in the string with the
# delimiter removed. Here is an example:

input = "1,2,3,4"
# read_delimited(input)  # --> ["1","2","3","4"]
# If input is an empty string, the result should be a list with a single
# empty string in it : [""]

# Please refer to the split  method for strings to see how to do this.

# Please complete the read_delimited function here.

# Answer

def read_delimited(line, separator=","):
    # your code here
    values = line.split(separator)  #-hide
    return values                   #-hide
