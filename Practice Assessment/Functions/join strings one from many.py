# One from many
# The join_strings returns a single string that is the
# concatenation of all of the input strings, separated by the separator
# if supplied. Here's an example:

join_strings(["aaa", "bbb", "ccc"], "--")
#     # Returns "aaa--bbb--ccc"
# Think through this problem. It may seem complex, but you can do this.

# What gets returned for an empty list?
# Can you do this with a single if statement?
# Can you do this with a for loop?

# Answer

def join_strings(strings, separator=""):
    # your code here
    if len(strings) == 0:               
        return ""
    output = strings[0]
    for string in strings[1:]:
        output += separator + string
    return output
