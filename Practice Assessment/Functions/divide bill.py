Implement the function divide_bill, which will return the amount
due for each diner given a total bill, number of diners, and the desired
tip amount.

bill	num_diners	tip	    output

10	    4	        0.0	    2.5
10	    4	        0.1	    2.75
30	    5	        0.2	    7.2

Answer

def divide_bill(bill, num_diners, tip=0.2):
    # your code here
    total = bill * (1 + tip)  #-hide
    return total / num_diners #-hide
