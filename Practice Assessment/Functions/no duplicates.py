No duplicates
The function below takes a list of items and
returns a copy of the list with all of the duplicate items removed in
the same order they were encountered in the list.

Examples:

input	    output
[]	        []
[1,2]	    [1,2]
[1,1,2]	    [1,2]
[1,2,1]	    [1,2]
[2,1,1]	    [2,1]
[1,1,1]	    [1]
Answer

def unique_elements(items):
    new_items = []
    for item in items:
        if item not in new_items:
            new_items.append(item)
    return new_items
