shift_cipher("b", 1)     # --> "c"
shift_cipher("b", 2)     # --> "d"
shift_cipher("b", -1)    # --> "a"
shift_cipher("bbc", 1)   # --> "ccd"
shift_cipher("bbc", -1)  # --> "aab"
shift_cipher("bbc", 3)   # --> "eef"

You can use the ord built-in method to turn a character into its corresponding number.

You can use the chr built-in method to turn a number into its corresponding character.

Think through this problem. It may seem complex, but you can do this.

What gets returned for an empty string?
Can you do this with a single if statement?
Can you do this with a for loop?
ord("a") # Returns 97, the integer value for the letter "a"
chr(97)  # Returns "a"
chr(98)  # Returns "b"
Answer

def shift_cipher(message, shift):
    # your code here
    encrypted_message = ""                       #-hide
    for index in range(len(message)):            #-hide
        char = message[index]                    #-hide
        ascii = ord(char)                        #-hide
        encrypted_message += chr(ascii + shift)  #-hide
    return encrypted_message                     #-hide
