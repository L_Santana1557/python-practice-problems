# Longest string
# This function takes a list of strings and returns the longest string in the list.

# Note: The input data will have only one longest string.


#    strings	                output
# []	                        None
# ["a"]	                         "a"
# ["a", "bbb", "cc"]	        "bbb"
Think through this problem. It may seem complex, but you can do this.

What gets returned for an empty list?
Can you do this with a single if statement?
Can you do this with a for loop?
Answer


def find_longest(strings):
    # your code here
    longest = None                #-hide
    max_length = 0                #-hide

    for string in strings:        #-hide
        length = len(string)      #-hide
        if length > max_length:   #-hide
            max_length = length   #-hide
            longest = string      #-hide
    return longest                #-hide
