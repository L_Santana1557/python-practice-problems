# What's the last item of []?
# There are a couple of problems with the last_item function:

# It's supposed to return the last item of the list passed in through the list parameter
# If the list parameter contains an empty list, then it should return None
# Update the code below so that it passes its unit tests.

Answer

def last_item(list):
    if len(list) == 0:
        return None
    last_item = list[-1]
    return last_item
