Total weight
This function takes a list of items in a shipment and
calculates the total weight of the shipment.
Below is an example shipment list in Python:

shipment = [
    {
        "product_name": "can of soup",
        "product_weight_pounds": 3.4,
        "quantity": 3,
    },
]
The total weight for the example above would be 3 x 3.4 = 10.2


def find_shipment_weight(shipment):
    # your code here
    total_weight = 0       
    for item in shipment:
        total_weight += item["product_weight_pounds"] * item["quantity"]
    return total_weight
