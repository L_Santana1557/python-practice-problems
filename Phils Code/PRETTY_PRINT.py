# Pretty Printing
# to make your dictionaries and nested lists easy to read, you can add this to the top of your code.

from pprint import pprint

# Then you can use pprint() like this

pprint(my_dictionary)
pprint(my_nested_list)

# You cannot add a label argument with pprint like you can in with print
# For example, this works

print("here is my result", result)

# This does not

pprint("here is my result", result)

# If you want to label your pprint statement, you have to do this

print("here is my result")
pprint(result)
