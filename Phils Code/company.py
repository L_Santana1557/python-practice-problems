company = [
    {"name": "leah",
     "position": "manager",
     "rate": 55,
     "hours": 15
    },
    {"name": "freddy",
     "position": "manager",
     "rate": 25,
     "hours": 30
    },
    {"name": "isaac",
     "rate": 20,
     "hours": 20
    },
    {"name": "murphey",
     "position": "worker",
     "rate": 41,
     "hours": 25
    },
    {"name": "phil",
     "position": "worker",
     "rate": 9,
     "hours": 45
    },
]


def get_list_of_managers(company):
    manager_list = []
    for person in company:
        if person.get("position") == "manager":
            manager_list.append(person.get("name"))
    return manager_list


def get_list_of_workers(company):
    worker_list = []
    for person in company:
        if person.get("position") == "worker":
            worker_list.append(person["name"])
    return worker_list


def longest_name(company):
    long_name = ""
    for employee in company:
        if len(employee.get("name")) > len(long_name):
            long_name = employee["name"]
    return long_name


def get_manager_pay(company):
    total = 0
    for person in company:
        if person.get("position") == "manager":
            total += person.get("rate") * person.get("hours")
    return total

def who_made_the_most(company):
    made_most = ""
    most_money = 0
    for employee in company:
        money = employee.get("rate") * employee.get("hours")
        if money > most_money:
            made_most = employee.get("name")
            most_money = money
    return made_most
