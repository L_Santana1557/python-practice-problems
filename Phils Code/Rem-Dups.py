nums = [2,4,6,8,10,12,3,5,6,6,7,8,9,7,5,5,89,8,6,78,9,87,5]

def remove_duplicates(lst):
    cache = {}
    result = []
    for item in lst:
        if item not in cache:
            cache[item] = True
            result.append(item)
    return result

print(remove_duplicates(nums))
